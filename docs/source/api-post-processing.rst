Post-processing
===============
*ddeq* provides functions for post-processing, which includes  methods for
estimating annual emissions from individual estimates, for converting NO2 to
NOx emissions, for visualizing the results and for writing data output in
standardized NetCDF format.


NO\ :sub:`2` to NO\ :sub:`x` conversion
---------------------------------------
To convert NO\ :sub:`2` to NO\ :sub:`x` emissions the following function can be
used that scales all fields starting with "NO2" that have units of "kg s-1" with
a scaling factor:

.. autofunction:: ddeq.emissions.convert_NO2_to_NOx_emissions


Annual emissions
----------------
Methods that are applied to individual images (i.e. GP, CSF, LCSF and IME)
provide emission estimates at measurement time. A time series of individual
estimates can be extrapolated to annual emissions fitting a seasonal cycle.
A detailed example is available `here <example_annual_emissions.html>`_. The
following function can be used

.. autofunction:: ddeq.timeseries.fit



Visualization
-------------
A large number of functions are provided that can be used to visualize the
results from the different methods. The functions below are the main function
for plotting the results of GP, CSF, LCSF and DIV method. More examples for
plotting results are shown in several Jupyter Notebooks.

.. autofunction:: ddeq.vis.plot_gauss_result

.. autofunction:: ddeq.vis.plot_csf_result

.. autofunction:: ddeq.vis.plot_lcsf_result

.. autofunction:: ddeq.vis.plot_divergence_maps