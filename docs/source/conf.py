# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import datetime
import importlib.metadata
import pathlib
import sys

sys.path.insert(0, pathlib.Path(__file__).parents[2].resolve().as_posix())

project = 'ddeq'
copyright = '2019-{year:04d}, Gerrit Kuhlmann et al.'.format(year=datetime.datetime.now().year)
author = 'Gerrit Kuhlmann'
release = importlib.metadata.version("ddeq")
version = importlib.metadata.version("ddeq")

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'nbsphinx',
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
]

templates_path = ['_templates']
exclude_patterns = []

# Don't show class signature with the class' name.
autodoc_class_signature = "separated"

# -- Options for Notebooks
nbsphinx_execute = 'always' # never, auto, always
nbsphinx_allow_errors = True

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'
html_static_path = ['_static']


# Avoid warnings in documentation
import ddeq
ddeq.vis.create_map(ddeq.smartcarb.DOMAIN, admin_level=1, edgecolor='k')