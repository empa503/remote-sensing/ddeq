Tutorial and examples
=====================
Jupyter Notebooks are provided with tutorials and examples. They can be found in
the notebooks folder in the git repository.

The examples below showcase some of ddeq's key features:

.. toctree::
   :maxdepth: 1

   tutorial-introduction-to-ddeq
   example_tropomi_Matimba
   example_annual_emissions