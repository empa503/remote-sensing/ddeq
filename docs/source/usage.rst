Usage
=====
How the different modules can be used is demonstrated in :doc:`tutorial`
provided with the library. The library consists of four main components
for data input, pre-processing, emission quantification and post processing.

.. toctree::
  :maxdepth: 2

  api-data-input
  api-pre-processing
  api-quantification
  api-post-processing
