Installation
============

Latest release
--------------

The *ddeq* Python library is hosted on
`Gitlab.com <https://gitlab.com/empa503/remote-sensing/ddeq>`_.

If you use pip, *ddeq* can be installed with:

.. code-block:: console

   $ pip install ddeq

To run the tutorials and examples, it is necessary to install 
`Jupyter <https://jupyter.org>`_, for example, with:

.. code-block:: console

    $ pip install jupyterlab

We currently do not provide a conda package. However, `pip` can be used to
install *ddeq* in a conda environment. On Windows, you might want to install
dependencies that require C libraries before installing *ddeq*. In the current
version, the only such dependency is `pycurl`, which can be installed with:

.. code-block:: console

    conda install pycurl


Development version
-------------------

The development version can be cloned from
`Gitlab.com <https://gitlab.com/empa503/remote-sensing/ddeq>`_ using

.. code::

    git clone https://gitlab.com/empa503/remote-sensing/ddeq.git

To install *ddeq*, you can use `pip`. Installing *ddeq* editable (`-e` options)
allows you to directly modify the cloned code.

.. code::

    cd ddeq
    pip install -e .