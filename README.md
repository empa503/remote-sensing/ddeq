# Data-driven emission quantification

ddeq is Python library for data-driven emission quantification of emission hot
spots such as cities, power plants and industrial facilities.

## Documentation

The full documentation is hosted at https://ddeq.readthedocs.io.

## Reference publication
Kuhlmann, G., Koene, E., Meier, S., Santaren, D., Broquet, G.,
Chevallier, F.,vHakkarainen, J., Nurmela, J., Amorós, L., Tamminen, J., and
Brunner, D.: The ddeq Python library for point source quantification from
remote sensing images (version 1.0), Geosci. Model Dev., 17, 4773–4789,
https://doi.org/10.5194/gmd-17-4773-2024, 2024.