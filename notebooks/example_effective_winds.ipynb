{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "81e71fb1-33a5-469a-a876-be6d56b90baf",
   "metadata": {},
   "source": [
    "# Example: Download ERA5 and compute effective wind fields\n",
    "In this notebook, we compare different methods for computing effective wind speeds from ERA5 data that are implemented in ddeq library."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "995a3958-4584-4e04-a3d4-c79fa6da7570",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import ddeq"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb77d7ea-063c-494d-ab1d-ae1ed3b0f64c",
   "metadata": {},
   "source": [
    "## Download ERA5 on model levels, pressure levels and ensemble spread\n",
    "**Important:** Downloading ERA-5 data requires setting up access for the Copernicus Climate Data Store (CDS) Application Program Interface (API). Link: https://cds.climate.copernicus.eu/api-how-to."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3f3bf72-4725-4148-b50f-ac34b568195c",
   "metadata": {},
   "source": [
    "First, we will download ERA5 data on model levels, pressure levels and on single level for the SMARTCARB model domain that covered part of Germany and Poland. Note that the download function will check if the file already exists and only download missing files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e1769737-cf4d-4afa-aba9-132aa4f75311",
   "metadata": {},
   "outputs": [],
   "source": [
    "# SMARTCARB domain (North, West, South, East)\n",
    "time = pd.Timestamp(\"2015-04-23\")\n",
    "\n",
    "area = {\n",
    "    \"north\": 55.5,\n",
    "    \"west\": 7.6,\n",
    "    \"south\": 49.5,\n",
    "    \"east\": 20.0\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "218546e3-43c6-4b2b-9f56-bf9f1234bb8f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Download single level such as 10-m wind speed and boundary layer height\n",
    "sl_filename = ddeq.era5dl.download_single_lvl(time=time, area=area, timesteps=24)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3aea9a27-5021-4dbc-9ee4-84ef40f4db37",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Download on pressure levels\n",
    "pl_filename = ddeq.era5dl.download_pressure_lvl(time=time, area=area, timesteps=24)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b91b6152-cb8e-495d-88ac-a7a3bbc4aa15",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Download on model levels (slow)\n",
    "ml_filename = ddeq.era5dl.download_model_lvl(time=time, area=area, timesteps=24)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3519504-58ff-47b7-97d5-bb90effd7c34",
   "metadata": {},
   "source": [
    "## Different approaches for computing the effective wind speed"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0f074a4-ac7a-4334-b855-30f67c99c719",
   "metadata": {},
   "source": [
    "We will show different approaches for computing the effective wind speed using ddeq. Note that the approch most suitable for an application depends on the emission quantification method and the remote sensing observations.\n",
    "\n",
    "All methods use `ddeq.era5.read` to open the wind dataset and compute the effective wind speed. Here, we will compute effective wind speed at the longitude/latitude location of sources in the SMARTCARB domain. The sources dataset is provded using the \"sources\". The function will interpolate the wind to the locations. If the sources parameter is None, `ddeq.era5.read` will return the effective wind speed on the model grid. It is also possible to provde \"times\", which will select the nearest neighboring times.\n",
    "\n",
    "The following example reads the 10m wind speeds without spatial interpolation, but times are selected and the extent is limited to the SMARTCARB area."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26a14319-9e87-4cfc-8a0c-9bacd2b52bb5",
   "metadata": {},
   "outputs": [],
   "source": [
    "ddeq.era5.read(\n",
    "    sl_filename,\n",
    "    method=\"10m\",\n",
    "    times=[pd.Timestamp(\"2015-04-23 11:00\"), pd.Timestamp(\"2015-04-23 12:00\")],\n",
    "    extent=area\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cf3a717e-96e4-4a70-ac65-ed6758478c90",
   "metadata": {},
   "source": [
    "Wind at 10m or 100m as effective wind speed, which is available from the single level product:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "81a5e82a-a9f8-4cdf-af33-da94ddd6e8bc",
   "metadata": {},
   "outputs": [],
   "source": [
    "sources = ddeq.sources.read_smartcarb()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0280dc25-7c68-481b-b38d-896a9e346dbd",
   "metadata": {},
   "outputs": [],
   "source": [
    "wind_10m = ddeq.era5.read(sl_filename, method=\"10m\", sources=sources)\n",
    "wind_100m = ddeq.era5.read(sl_filename, method=\"100m\", sources=sources)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cfd40684-a4b3-4329-87fb-b25014c1f39f",
   "metadata": {},
   "source": [
    "It is possible to calculate the effective wind speed by averaging different vertical levels on pressure or model levels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6fc14af7-e2ba-4991-89b6-c246909821e0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Wind on lowest four pressure levels:\n",
    "wind_pres = ddeq.era5.read(\n",
    "    lvl_filename=pl_filename,\n",
    "    method=\"levels\",\n",
    "    levels=[1000,  975,  950,  925],\n",
    "    sources=sources\n",
    ")\n",
    "\n",
    "# Wind at first index (i.e. lowest level for pressure levels):\n",
    "wind_ll = ddeq.era5.read(\n",
    "    lvl_filename=pl_filename,\n",
    "    method=\"levels\", level_units=\"index\",\n",
    "    levels=0,\n",
    "    sources=sources\n",
    ")\n",
    "\n",
    "# Wind at last model level (i.e. lowest level for model levels):\n",
    "wind_137 = ddeq.era5.read(\n",
    "    lvl_filename=ml_filename,\n",
    "    method=\"levels\",\n",
    "    levels=[-1], level_units=\"index\",\n",
    "    sources=sources\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b417ddc-bfca-416c-aade-fff70557b58b",
   "metadata": {},
   "source": [
    "Effective wind speed can be computed by weighting the GNFR-A emission profile. Since the vertical resolution on pressure levels is limited, we add 10 and 100m winds to the pressure levels to better represent the profile:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d0c1fd8d-2b13-4baf-bf9b-b617ec43c690",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Weighted using model levels\n",
    "wind_gnfra_ml = ddeq.era5.read(\n",
    "    sng_filename=sl_filename,\n",
    "    lvl_filename=ml_filename,\n",
    "    method=\"gnfra\",\n",
    "    sources=sources\n",
    ")\n",
    "\n",
    "# Weighted using pressure level plus 10m and 100m winds\n",
    "wind_gnfra_pl = ddeq.era5.read(\n",
    "    sng_filename=sl_filename,\n",
    "    lvl_filename=pl_filename,\n",
    "    method=\"gnfra\",\n",
    "    sources=sources\n",
    ")\n",
    "\n",
    "# Check different between model and pressure levels:\n",
    "d = wind_gnfra_pl.speed.values - wind_gnfra_ml.speed.values\n",
    "print(f\"MB: {np.mean(d):0.3f} m/s\") # MB: -0.030 m/s\n",
    "print(f\"SD: {np.std(d):0.3f} m/s\")  # SD: 0.269 m/s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b60ade79-8cda-4704-9e68-d4918aaaf2c0",
   "metadata": {},
   "source": [
    "It is also possible to use the mean wind in the boundary layer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9258596a-20c5-4cf1-afb7-d532ca08c3e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "wind_pbl_mean_pl = ddeq.era5.read(\n",
    "    sng_filename=sl_filename,\n",
    "    lvl_filename=pl_filename,\n",
    "    method=\"blh-mean\",\n",
    "    sources=sources\n",
    ")\n",
    "\n",
    "wind_pbl_mean_ml = ddeq.era5.read(\n",
    "    sng_filename=sl_filename,\n",
    "    lvl_filename=ml_filename,\n",
    "    method=\"blh-mean\",\n",
    "    sources=sources\n",
    ")\n",
    "\n",
    "# Check different between model and pressure levels:\n",
    "d = wind_pbl_mean_pl.speed.values - wind_pbl_mean_ml.speed.values\n",
    "print(f\"MB: {np.nanmean(d):0.3f} m/s\") # MB: -0.025 m/s\n",
    "print(f\"SD: {np.nanstd(d):0.3f} m/s\")  # SD: 0.193 m/s\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90ae5487-c5a1-456f-b6d7-2555dfdf5e7d",
   "metadata": {},
   "source": [
    "The wind can also be computed at a single height:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b01b6bb7-c5e6-45b4-8175-a08934a2dcf7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# at 250 m\n",
    "wind_250m_pl = ddeq.era5.read(\n",
    "    sng_filename=sl_filename,\n",
    "    lvl_filename=pl_filename,\n",
    "    method=\"height\", heights=250.0,\n",
    "    sources=sources\n",
    ")\n",
    "\n",
    "wind_250m_ml = ddeq.era5.read(\n",
    "    sng_filename=sl_filename,\n",
    "    lvl_filename=ml_filename,\n",
    "    method=\"height\", heights=250.0,\n",
    "    sources=sources\n",
    ")\n",
    "\n",
    "d = wind_250m_pl.speed.values - wind_250m_ml.speed.values\n",
    "print(f\"MB: {np.nanmean(d):0.3f} m/s\") # MB: -0.176 m/s\n",
    "print(f\"SD: {np.nanstd(d):0.3f} m/s\")  # SD: 0.246 m/s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b470a23-344e-4824-b399-7b6c16a09da3",
   "metadata": {},
   "source": [
    "It is also possible to interpolate the wind speed at the middle of the planatary boundary layer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8bc8a15-781c-4b83-9cd3-b18af6e8ab07",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Wind speed at middle of planet boundary layer\n",
    "wind_pbl_mid_pl = ddeq.era5.read(\n",
    "    sng_filename=sl_filename,\n",
    "    lvl_filename=pl_filename,\n",
    "    method=\"pbl-mid\",\n",
    "    sources=sources\n",
    ")\n",
    "\n",
    "wind_pbl_mid_ml = ddeq.era5.read(\n",
    "    sng_filename=sl_filename,\n",
    "    lvl_filename=ml_filename,\n",
    "    method=\"pbl-mid\",\n",
    "    sources=sources\n",
    ")\n",
    "\n",
    "d = wind_pbl_mid_pl.speed.values - wind_pbl_mid_ml.speed.values\n",
    "print(f\"MB: {np.nanmean(d):0.3f} m/s\") # MB: -0.038 m/s\n",
    "print(f\"SD: {np.nanstd(d):0.3f} m/s\")  # SD: 0.240 m/s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27941ffc-397e-42ca-8595-eff6d2b183b4",
   "metadata": {},
   "source": [
    "## Visualize wind processing by ddeq library\n",
    "The following code reads the ERA5 files for the SMARTCARB model domain on 23 April 2015. It visualizes the U-wind profile for pressure and height above surface. Note that for ERA5 on pressure levels, 10m and 100m winds are added to increase the resolution near the surface."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c48ec876-234a-45e9-821e-c2ab90a0ceaf",
   "metadata": {},
   "outputs": [],
   "source": [
    "def read_at_source(source=\"Janschwalde\", hour=12):\n",
    "\n",
    "    # Get source location\n",
    "    lon, lat, _ = ddeq.sources.get_location(sources, \"Janschwalde\")\n",
    "\n",
    "    # Open and prepare ERA5 on pressure, model and single level\n",
    "    pl = ddeq.era5.open(pl_filename)\n",
    "    ml = ddeq.era5.open(ml_filename)\n",
    "    sl = ddeq.era5.open(sl_filename)\n",
    "\n",
    "    # Interpolate to Janschwalde power plant at 12 UTC\n",
    "    sl = sl.interp(lon=lon, lat=lat, method=\"linear\").isel(time=hour)\n",
    "    ml = ml.interp(lon=lon, lat=lat, method=\"linear\").isel(time=hour)\n",
    "    pl = pl.interp(lon=lon, lat=lat, method=\"linear\").isel(time=hour)\n",
    "\n",
    "    # Compute heights for model levels and stack wind from pressure levels and 10/100 m\n",
    "    ml, sl = ddeq.era5.compute_height_levels(ml, sl)\n",
    "    psl = ddeq.era5.stack_pressure_and_single_levels(pl, sl)\n",
    "\n",
    "    return pl, ml, sl, psl"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a47ae2e2-84ad-4b12-a20c-858f2dc49751",
   "metadata": {},
   "outputs": [],
   "source": [
    "pl, ml, sl, psl = read_at_source(source=\"Janschwalde\", hour=12)\n",
    "\n",
    "# Plot profiles\n",
    "fig, axes = plt.subplots(1,2, figsize=(6,6))\n",
    "\n",
    "axes[0].plot(ml.u, ml.p_mid / 1e2, \"o-\", label=\"pressure levels\")\n",
    "axes[1].plot(ml.u, ml.h, \"o-\", label=\"model levels\")\n",
    "\n",
    "axes[0].plot(pl.u, pl.pressure_level, \"s-\", label=\"model levels\")\n",
    "axes[1].plot(psl.u, psl.h, \"s-\", label=\"pres. levels plus\\n10m and 100m wind\")\n",
    "\n",
    "axes[0].set_ylim(sl.sp/1e2, 850)\n",
    "axes[1].set_ylim(0, 1000)\n",
    "\n",
    "axes[0].set_ylabel(\"Pressure [hPa]\")\n",
    "axes[1].set_ylabel(\"Height above ground [m]\")\n",
    "\n",
    "for ax in axes:\n",
    "    ax.set_xlabel(\"U component [m/s]\")\n",
    "    ax.set_xlim(-5,10)\n",
    "    ax.legend()\n",
    "\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1948f20a-fc85-47b5-a20f-c880a8970112",
   "metadata": {},
   "outputs": [],
   "source": [
    "hour = 12\n",
    "source = \"Janschwalde\"\n",
    "pl, ml, sl, psl = read_at_source(source=source)\n",
    "\n",
    "fig, ax = plt.subplots(1,1, figsize=(5,7))\n",
    "ax.plot(np.sqrt(ml.u**2 + ml.v**2), ml.h, \"o-\", label=\"model levels\")\n",
    "ax.plot(np.sqrt(psl.u**2 + psl.v**2), psl.h, \"s-\", label=\"pressure levels\")\n",
    "\n",
    "ax.hlines(sl.blh, -5, 10, colors=\"k\", ls=\"--\", label=\"PBL\")\n",
    "\n",
    "for w, h, marker, label in [\n",
    "    (wind_10m, 10.0, \"bv\", \"10/100/250 m\"),\n",
    "    (wind_100m, 100.0, \"bv\", None),\n",
    "    (wind_250m_ml, 250.0, \"bv\", None),\n",
    "    (wind_250m_pl, 250.0, \"bv\", None),\n",
    "    (wind_pbl_mid_pl, sl.blh/2, \"r^\", \"PBL mid\"),\n",
    "    (wind_pbl_mid_ml, sl.blh/2, \"r^\", None),\n",
    "]:\n",
    "    ax.plot(w[\"speed\"].sel(source=source).isel(time=hour), h, marker, label=label)\n",
    "\n",
    "ax.vlines(wind_gnfra_ml[\"speed\"].sel(source=\"Janschwalde\").isel(time=hour), 170, 990, colors=\"green\", label=\"GNFR-A\")\n",
    "ax.vlines(wind_gnfra_pl[\"speed\"].sel(source=\"Janschwalde\").isel(time=hour), 170, 990, colors=\"green\", )\n",
    "\n",
    "ax.vlines(wind_pbl_mean_pl[\"speed\"].sel(source=\"Janschwalde\").isel(time=hour), 0.0, sl.blh, colors=\"red\", label=\"PBL mean\")\n",
    "ax.vlines(wind_pbl_mean_ml[\"speed\"].sel(source=\"Janschwalde\").isel(time=hour), 0.0, sl.blh, colors=\"red\")\n",
    "\n",
    "ax.vlines(wind_pres[\"speed\"].sel(source=\"Janschwalde\").isel(time=hour), 0.0, 800.0, colors=\"blue\", label=\"lowest pressure levels\")\n",
    "\n",
    "ax.grid()\n",
    "ax.set_ylim(0, 1200)\n",
    "ax.legend(fontsize=\"small\", ncol=2)\n",
    "ax.set_ylabel(\"Height above ground [m]\")\n",
    "ax.set_xlabel(\"Wind speed [m/s]\")\n",
    "ax.set_xlim(2,5);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3eb32bff-edef-4c0e-8798-83f6a33206b5",
   "metadata": {},
   "outputs": [],
   "source": [
    "hours = np.arange(24)\n",
    "pl, ml, sl, psl = read_at_source(source=source, hour=hours)\n",
    "\n",
    "\n",
    "plt.plot(hours, wind_10m[\"speed\"].sel(source=\"Janschwalde\"), 'o-', color=\"C0\", label=\"10 m\")\n",
    "plt.plot(hours, wind_100m[\"speed\"].sel(source=\"Janschwalde\"), 's-', color=\"C1\", label=\"100 m\")\n",
    "\n",
    "plt.plot(hours, wind_250m_ml[\"speed\"].sel(source=\"Janschwalde\"), 'v-', color=\"C2\", label=\"250 m\")\n",
    "plt.plot(hours, wind_250m_pl[\"speed\"].sel(source=\"Janschwalde\"), 'v--', color=\"C2\")\n",
    "\n",
    "plt.plot(hours, wind_pbl_mean_ml[\"speed\"].sel(source=\"Janschwalde\"), '^-', color=\"C3\", label=\"PBL mean\")\n",
    "plt.plot(hours, wind_pbl_mean_pl[\"speed\"].sel(source=\"Janschwalde\"), '^--', color=\"C3\")\n",
    "\n",
    "plt.plot(hours, wind_pbl_mid_ml[\"speed\"].sel(source=\"Janschwalde\"), '>-', color=\"C4\", label=\"PBL mid\")\n",
    "plt.plot(hours, wind_pbl_mid_pl[\"speed\"].sel(source=\"Janschwalde\"), '>--', color=\"C4\")\n",
    "\n",
    "plt.plot(hours, wind_gnfra_ml[\"speed\"].sel(source=\"Janschwalde\"), 'd-', color=\"C5\", label=\"GNFR-A\")\n",
    "plt.plot(hours, wind_gnfra_pl[\"speed\"].sel(source=\"Janschwalde\"), 'd--', color=\"C5\")\n",
    "\n",
    "plt.plot(hours, wind_pres[\"speed\"].sel(source=\"Janschwalde\"), 'x-', color=\"C6\", label=\"four lowest pressure levels\")\n",
    "\n",
    "plt.plot([], [], \"k--\", label=\"PBL height\")\n",
    "\n",
    "plt.xlabel(\"Hour of day [UTC]\")\n",
    "plt.ylabel(\"Effective wind speed [m/s]\")\n",
    "plt.legend(ncol=2)\n",
    "plt.ylim(0,10)\n",
    "plt.grid()\n",
    "\n",
    "plt.twinx()\n",
    "\n",
    "lon, lat, _ = ddeq.sources.get_location(sources, \"Janschwalde\")\n",
    "plt.plot(hours, sl.blh, \"k--\")\n",
    "plt.ylim(0,2000)\n",
    "plt.yticks(np.arange(0,2001,400));\n",
    "plt.ylabel(\"PBL height [m]\")\n",
    "\n",
    "plt.title(\"Jänschwalde on 23 April 2015\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
